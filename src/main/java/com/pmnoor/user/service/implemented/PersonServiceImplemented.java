package com.pmnoor.user.service.implemented;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import com.pmnoor.model.Person;
import com.pmnoor.repository.PersonRepository;
import com.pmnoor.services.PersonService;

@Service
public class PersonServiceImplemented implements PersonService {

	@Autowired
	private PersonRepository noor;

	public List<Person> getAllPersons() {
		List<Person> persons = new ArrayList<Person>();
		noor.findAll().forEach(person -> persons.add(person));
		return persons;
	}

	public Person getPersonById(int id) {
		return noor.findById(id).get();
	}

	public void saveOrUpdate(Person person) {
		noor.save(person);
	}

	public void delete(int id) {
		noor.deleteById(id);
	}

}
