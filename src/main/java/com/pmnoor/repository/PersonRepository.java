package com.pmnoor.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.pmnoor.model.Person;

public interface PersonRepository extends JpaRepository<Person, Integer>  {
	

}
